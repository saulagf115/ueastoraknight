// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FloorSwitch.generated.h"

UCLASS()
class ASTORAKNIGHT_API AFloorSwitch : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFloorSwitch();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Floor Switch")
	class UStaticMeshComponent* Door;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Floor Switch")
		class UStaticMeshComponent* FloorSwitch;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Floor Switch")
	class UBoxComponent* TriggerBox;


	UPROPERTY(BlueprintReadWrite,Category="Floor Switch")
	FVector InitialDoorLocation;

	UPROPERTY(BlueprintReadWrite, Category = "Floor Switch")
	FVector InitialFloorSwitchLocation;

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	//Behaviour Door and Floor Switch Events
	UFUNCTION(BlueprintImplementableEvent,Category="Floor Switch")
	void RaiseDoor();

	UFUNCTION(BlueprintImplementableEvent,Category="Floor Switch")
	void LowerDoor();

	UFUNCTION(BlueprintImplementableEvent,Category="Floor Switch")
	void RaiseFloorSwitch();

	UFUNCTION(BlueprintImplementableEvent, Category = "Floor Switch")
		void LowerFloorSwitch();

	UFUNCTION(BlueprintCallable,Category="Floor Switch")
	void UpdateDoorLocation(float Z);

	UFUNCTION(BlueprintCallable, Category = "Floor Switch")
	void UpdateFloorSwitchLocation(float Z);

};
