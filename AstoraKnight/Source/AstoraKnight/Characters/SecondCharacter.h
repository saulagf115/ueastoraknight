// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SecondCharacter.generated.h"

UCLASS()
class ASTORAKNIGHT_API ASecondCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASecondCharacter();

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Camera")
	class USpringArmComponent* SpringArmComponent;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Camera")
	class UCameraComponent* CameraComponent;

	class UMyMovementComponent* MyMovement;

	virtual UPawnMovementComponent* GetMovementComponent() const override;

	FORCEINLINE USpringArmComponent* GetSpringArm(){return SpringArmComponent;}
	FORCEINLINE void SetSpringArm(USpringArmComponent* SpringArm){SpringArmComponent = SpringArm;}

	FORCEINLINE UCameraComponent* GetCamera() { return CameraComponent; }
	FORCEINLINE void SetCamera(UCameraComponent* Camera) { CameraComponent = Camera; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:

	void MoveForward(float Input);

	void MoveRight(float input);

	void CameraX(float AxisValue);

	void CameraY(float AxisValue);

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="Camera",meta=(AllowPrivateAccess = "true"))
	FVector2D CameraInput;

};
