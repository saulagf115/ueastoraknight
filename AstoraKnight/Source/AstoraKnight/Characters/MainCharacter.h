// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MainCharacter.generated.h"

UCLASS()
class ASTORAKNIGHT_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMainCharacter();

	/*
		Camera Boom positioning the camera behind the player
	*/
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category=Camera, meta = (AllowPrivateAcess = "true"))
	class USpringArmComponent* CameraBoom;


	//follow camera 
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category = Camera,meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	//Base Turn Rate to scale turning function for the camera
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category=Camera)
	float BaseTurnRate;

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category=Camera)
	float BaseLookUpRate;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void MoveForward(float Value);

	void MoveRight(float Value);


	//called via input to turn at a given rate
	//@param Rate This is a normalized rate, i.e 1.0 means 100% of desired turn rate  
	void TurnAtRate(float Rate);


	//called via input to look up/down at a given rate
	//@param Rate This is a normalized rate, i.e 1.0 means 100% of desired look up rate 
	void LookUpAtRate(float Rate);

	FORCEINLINE class USpringArmComponent* GetCameraBoom(){return CameraBoom;}
	FORCEINLINE class UCameraComponent* GetFollowCamera() {return FollowCamera;}

};
