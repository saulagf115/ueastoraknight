// Fill out your copyright notice in the Description page of Project Settings.


#include "SecondCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/InputComponent.h"
#include "Camera/CameraComponent.h"
#include "../Colliders/MyMovementComponent.h"

// Sets default values
ASecondCharacter::ASecondCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArmComponent->TargetArmLength = 600.f;
	SpringArmComponent->bEnableCameraLag = true;
	SpringArmComponent->CameraLagSpeed = 3.f;
	SpringArmComponent->SetupAttachment(GetRootComponent());

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(SpringArmComponent);

	MyMovement = CreateDefaultSubobject<UMyMovementComponent>(TEXT("OwnPawnMovementComponent"));
	MyMovement->UpdatedComponent = RootComponent;


	CameraInput = FVector2D(0.f,0.f);

}

UPawnMovementComponent* ASecondCharacter::GetMovementComponent() const
{
	return MyMovement;
}

// Called when the game starts or when spawned
void ASecondCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASecondCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FRotator NewRotation = GetActorRotation();
	NewRotation.Yaw += CameraInput.X;
	SetActorRotation(NewRotation);

	FRotator NewSpringArmRotation = SpringArmComponent->GetComponentRotation();
	NewSpringArmRotation.Pitch = FMath::Clamp(NewSpringArmRotation.Pitch += CameraInput.Y,-80.f,-15.f);
	
	SpringArmComponent->SetWorldRotation(NewSpringArmRotation);

}

// Called to bind functionality to input
void ASecondCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"),this,&ASecondCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ASecondCharacter::MoveRight);

	PlayerInputComponent->BindAxis(TEXT("CameraX"), this, &ASecondCharacter::CameraX);
	PlayerInputComponent->BindAxis(TEXT("CameraY"), this, &ASecondCharacter::CameraY);

}


void ASecondCharacter::MoveForward(float Input)
{
	FVector Forward = GetActorForwardVector();

	if (MyMovement)
	{
		MyMovement->AddInputVector(Forward * Input);
	}
}

void ASecondCharacter::MoveRight(float input)
{
	FVector Right = GetActorRightVector();

	if (MyMovement)
	{
		MyMovement->AddInputVector(Right * input);
	}
}

void ASecondCharacter::CameraX(float AxisValue)
{
	CameraInput.X = AxisValue;
}

void ASecondCharacter::CameraY(float AxisValue)
{
	CameraInput.Y = AxisValue;
}
