// Copyright Epic Games, Inc. All Rights Reserved.

#include "AstoraKnight.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, AstoraKnight, "AstoraKnight" );
