// Fill out your copyright notice in the Description page of Project Settings.


#include "MyMovementComponent.h"

void UMyMovementComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime,TickType,ThisTickFunction);

	if (!PawnOwner || !UpdatedComponent || ShouldSkipUpdate(DeltaTime))
	{
		return;
	}

	FVector DesiredMovementThisFrame = ConsumeInputVector().GetClampedToMaxSize(1.f);

	if (!DesiredMovementThisFrame.IsNearlyZero())
	{
		FHitResult hit;

		SafeMoveUpdatedComponent(DesiredMovementThisFrame,UpdatedComponent->GetComponentRotation(),true,hit);

		//if we bump, into something, slide along the side of it
		if (hit.IsValidBlockingHit())
		{
			SlideAlongSurface(DesiredMovementThisFrame,1.f - hit.Time,hit.Normal,hit);
		}
	}
}