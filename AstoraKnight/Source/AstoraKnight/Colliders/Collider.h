// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Collider.generated.h"

UCLASS()
class ASTORAKNIGHT_API ACollider : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACollider();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Mesh")
	class UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Sphere")
	class USphereComponent* SphereComponent;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="SpringArm")
	class USpringArmComponent* SpringArmComponent;

	UPROPERTY(EditDefaultsOnly,Category="Camera")
	class UCameraComponent* CameraComponent;

	//my own pawn movement component
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category="Movement")
	class UMyMovementComponent* OwnMovementComponent;

	virtual UPawnMovementComponent* GetMovementComponent() const override;

	//Get and Set the MeshComponent
	FORCEINLINE UStaticMeshComponent* GetMeshComponent() { return MeshComponent; }
	FORCEINLINE void SetMeshComponent(UStaticMeshComponent* Mesh) { MeshComponent = Mesh; }

	//GET and Set the SpringArmComponent
	FORCEINLINE USpringArmComponent* GetSpringArmComponent(){return SpringArmComponent;}
	FORCEINLINE void SetSpringArmComponent(USpringArmComponent* SpringArm){ SpringArmComponent = SpringArm;}

	//GET and Set the SphereComponent
	FORCEINLINE USphereComponent* GetSphereComponent() { return SphereComponent; }
	FORCEINLINE void SetSphereComponent(USphereComponent* Sphere) { SphereComponent = Sphere; }

	//GET and Set the CameraComponent
	FORCEINLINE UCameraComponent* GetCameraComponent() { return CameraComponent; }
	FORCEINLINE void SetCameraComponent(UCameraComponent* Camera) { CameraComponent = Camera; }

private:
	
	//Functions to Input Component
	void MoveForward(float Input);
	void MoveRight(float Input);

	//Function to Move the Camera with the mouse X and Y axis
	void PitchCamera(float AxisValue);
	void YawCamera(float AxisValue);


	FVector2D CameraInput;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Speed", meta = (AllowPrivateAccess = "true"))
	float MaxSpeed;
};
