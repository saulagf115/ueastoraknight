// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AstoraKnightGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ASTORAKNIGHT_API AAstoraKnightGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
