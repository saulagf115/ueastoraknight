// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Critter.generated.h"

UCLASS()
class ASTORAKNIGHT_API ACritter : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACritter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Mesh")
	class USkeletalMeshComponent* Mesh;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Camera")
	class UCameraComponent* Camera;


private:

	void MoveForward(float Input);

	void MoveRight(float Input);

	FVector CurrentVelocity;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Speed",meta=(AllowPrivateAccess = "true"))
	float MaxSpeed;

};
