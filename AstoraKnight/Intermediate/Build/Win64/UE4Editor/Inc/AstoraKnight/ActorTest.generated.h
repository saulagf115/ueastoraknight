// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASTORAKNIGHT_ActorTest_generated_h
#error "ActorTest.generated.h already included, missing '#pragma once' in ActorTest.h"
#endif
#define ASTORAKNIGHT_ActorTest_generated_h

#define AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h_12_SPARSE_DATA
#define AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h_12_RPC_WRAPPERS
#define AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAActorTest(); \
	friend struct Z_Construct_UClass_AActorTest_Statics; \
public: \
	DECLARE_CLASS(AActorTest, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AstoraKnight"), NO_API) \
	DECLARE_SERIALIZER(AActorTest)


#define AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAActorTest(); \
	friend struct Z_Construct_UClass_AActorTest_Statics; \
public: \
	DECLARE_CLASS(AActorTest, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AstoraKnight"), NO_API) \
	DECLARE_SERIALIZER(AActorTest)


#define AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AActorTest(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AActorTest) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AActorTest); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AActorTest); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AActorTest(AActorTest&&); \
	NO_API AActorTest(const AActorTest&); \
public:


#define AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AActorTest(AActorTest&&); \
	NO_API AActorTest(const AActorTest&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AActorTest); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AActorTest); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AActorTest)


#define AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h_12_PRIVATE_PROPERTY_OFFSET
#define AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h_9_PROLOG
#define AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h_12_PRIVATE_PROPERTY_OFFSET \
	AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h_12_SPARSE_DATA \
	AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h_12_RPC_WRAPPERS \
	AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h_12_INCLASS \
	AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h_12_PRIVATE_PROPERTY_OFFSET \
	AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h_12_SPARSE_DATA \
	AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h_12_INCLASS_NO_PURE_DECLS \
	AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASTORAKNIGHT_API UClass* StaticClass<class AActorTest>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID AstoraKnight_Source_AstoraKnight_Actors_ActorTest_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
