// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASTORAKNIGHT_AstoraKnightGameModeBase_generated_h
#error "AstoraKnightGameModeBase.generated.h already included, missing '#pragma once' in AstoraKnightGameModeBase.h"
#endif
#define ASTORAKNIGHT_AstoraKnightGameModeBase_generated_h

#define AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h_15_SPARSE_DATA
#define AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h_15_RPC_WRAPPERS
#define AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAstoraKnightGameModeBase(); \
	friend struct Z_Construct_UClass_AAstoraKnightGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AAstoraKnightGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/AstoraKnight"), NO_API) \
	DECLARE_SERIALIZER(AAstoraKnightGameModeBase)


#define AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAAstoraKnightGameModeBase(); \
	friend struct Z_Construct_UClass_AAstoraKnightGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AAstoraKnightGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/AstoraKnight"), NO_API) \
	DECLARE_SERIALIZER(AAstoraKnightGameModeBase)


#define AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAstoraKnightGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAstoraKnightGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAstoraKnightGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAstoraKnightGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAstoraKnightGameModeBase(AAstoraKnightGameModeBase&&); \
	NO_API AAstoraKnightGameModeBase(const AAstoraKnightGameModeBase&); \
public:


#define AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAstoraKnightGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAstoraKnightGameModeBase(AAstoraKnightGameModeBase&&); \
	NO_API AAstoraKnightGameModeBase(const AAstoraKnightGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAstoraKnightGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAstoraKnightGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAstoraKnightGameModeBase)


#define AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h_12_PROLOG
#define AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h_15_SPARSE_DATA \
	AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h_15_RPC_WRAPPERS \
	AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h_15_INCLASS \
	AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h_15_SPARSE_DATA \
	AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASTORAKNIGHT_API UClass* StaticClass<class AAstoraKnightGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID AstoraKnight_Source_AstoraKnight_AstoraKnightGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
