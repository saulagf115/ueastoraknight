// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AstoraKnight/AstoraKnightGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAstoraKnightGameModeBase() {}
// Cross Module References
	ASTORAKNIGHT_API UClass* Z_Construct_UClass_AAstoraKnightGameModeBase_NoRegister();
	ASTORAKNIGHT_API UClass* Z_Construct_UClass_AAstoraKnightGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_AstoraKnight();
// End Cross Module References
	void AAstoraKnightGameModeBase::StaticRegisterNativesAAstoraKnightGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_AAstoraKnightGameModeBase_NoRegister()
	{
		return AAstoraKnightGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_AAstoraKnightGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AAstoraKnightGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_AstoraKnight,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAstoraKnightGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "AstoraKnightGameModeBase.h" },
		{ "ModuleRelativePath", "AstoraKnightGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AAstoraKnightGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAstoraKnightGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AAstoraKnightGameModeBase_Statics::ClassParams = {
		&AAstoraKnightGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AAstoraKnightGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AAstoraKnightGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AAstoraKnightGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AAstoraKnightGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AAstoraKnightGameModeBase, 3274056775);
	template<> ASTORAKNIGHT_API UClass* StaticClass<AAstoraKnightGameModeBase>()
	{
		return AAstoraKnightGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AAstoraKnightGameModeBase(Z_Construct_UClass_AAstoraKnightGameModeBase, &AAstoraKnightGameModeBase::StaticClass, TEXT("/Script/AstoraKnight"), TEXT("AAstoraKnightGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAstoraKnightGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
